/* funkcija pretraga, tj. algoritam pretrage sa stranice w3 school je isproban i testiran vise puta na stranicu godina
    i nakon toga unesen u pretraga.js
*/

function pretragaPredmet(){
// deklaracija varijabli
var tabela = document.getElementById("tabela1");  // uzimamo tabelu iz dokumenta
var unos = document.getElementById("unosPredmet");  // uzimamo input unos
var unos_Velika_slova = unos.value.toUpperCase();  // pretvaramo unos preko kojeg filtriramo tabelu na velika slova
var tr = tabela.getElementsByTagName("tr");   // iz tabele uzimamo tr vrijednost

var i, td;
for(i=0;i<tr.length;i++){  // krecemo se kroz tr vrijednost
    td = tr[i].getElementsByTagName("td")[0];  // preuzimamo prvu kolonu u tabeli
    if(td){ // ako postoji td vrijednost
        vrijednost = td.innerText || td.textContent ; // uzimamo broj pozicije td elementa tj. prve kolone
        if(vrijednost.toUpperCase().indexOf(unos_Velika_slova)>-1){ 
            // trazimo da li prva kolona (uvecena slova) sadrzi nas unos (velika slova) 
            // tj. indexOf nama vraca broj pozicije ako sadrzi nas unos
            tr[i].style.display="";
        }
        else {
            tr[0].style.display=""; // bez obzira ima li predmet iz prve godine ili ne
            tr[14].style.display="";  // forsiramo prikaz naziva prve i druge godine

            tr[i].style.display="none";
        }
    }
}
}

function pretragaNastavnik(){
    // deklaracija varijabli
    var tabela = document.getElementById("tabela1");  // uzimamo tabelu iz dokumenta
    var unos = document.getElementById("unosNastavnik");  // uzimamo input unos
    var unos_Velika_slova = unos.value.toUpperCase();  // pretvaramo unos preko kojeg filtriramo tabelu na velika slova
    var tr = tabela.getElementsByTagName("tr");   // iz tabele uzimamo tr vrijednost
    
    var i, td;
    for(i=0;i<tr.length;i++){  // krecemo se kroz tr vrijednost
        td = tr[i].getElementsByTagName("td")[2];  // preuzimamo prvu kolonu u tabeli
        if(td){ // ako postoji td vrijednost
            vrijednost = td.innerText || td.textContent ; // uzimamo broj pozicije td elementa tj. prve kolone
            if(vrijednost.toUpperCase().indexOf(unos_Velika_slova)>-1){ 
                // trazimo da li prva kolona (uvecena slova) sadrzi nas unos (velika slova) 
                // tj. indexOf nama vraca broj pozicije ako sadrzi nas unos
                tr[i].style.display="";
            }
            else {
                tr[i].style.display="none";
            }
        }
    }
    }

    function pretragaGodina(){
        // deklaracija varijabli
        var tabela1 = document.getElementById("tabela1");  // uzimamo tabelu iz dokumenta
        var unos = document.getElementById("unosGodine");  // uzimamo input unos
        var tr = tabela1.getElementsByTagName("tr");   // iz tabele uzimamo tr vrijednost
        
        var i=0;
        var vrijednost;
 
        if(unos.value==1){  
            for(i=14;i>=14;i++){  // napravljena petlja koja uklanja td-elemente od 14 elementa(tada pocinje druga godina) tj. od prikazuje listu svih redova prve godine
                tr[i].style.display="none";
            }
        }
          
        if(unos.value==2){   
            for(i=0;i<=13;i++){ // napravljena petlja koja uklanja td-elemente od 1 elementa do 14 elementa ( prva godina)  tj. od prikazuje listu svih redova druge godine
                tr[i].style.display="none";
            }
        }

        }