//Zadatak 4
function oboji(a,b,c,d){ /* u funkciji imamo 4 parametra, zato sto je max broj predmeta (u nasem slucaju ) koji jedan profesor predaje 4*/        
  
    // svaki predmet odnosno parametar nazivamo odredjenom varijablom
    var r1 = document.getElementById(a); 
    var r2 = document.getElementById(b);
    var r3 = document.getElementById(c);
    var r4 = document.getElementById(d);
    // u slucaju da imamo manje predmeta, parametri ce tacno popunit odredjenu poziciju varijable, tako da necemo imat problema ako je neka varijabla prazna

    // bojimo pozadinu redova zutom bojom
    r1.style.backgroundColor="yellow";
    r2.style.backgroundColor="yellow";
    r3.style.backgroundColor="yellow";
    r4.style.backgroundColor="yellow";
}
    // funckija vraca prvobitnu boju pozadine reda kada pomjerimo miš sa ćelije 
function vrati(a,b,c,d){  //  // kao u prethodnoj funkciji uzimamo na isti nacin id predmeta i pravimo varijable
    var r1 = document.getElementById(a);
    var r2 = document.getElementById(b);
    var r3 = document.getElementById(c);
    var r4 = document.getElementById(d);
   
    // vracamo prvobitnu boju pozadine
    r1.style.backgroundColor="";
    r2.style.backgroundColor="";
    r3.style.backgroundColor="";
    r4.style.backgroundColor="";
}

//Zadatak 1
function povecaj(x,y){ /* Funkcija prima dva parametra, jedan je cijeli div element od jedno člana osoblja,
                                                 a drugi parametar je slika odnosno logo osobe (koja se treba prosirit)*/

    // parametre nazivamo varijablama "box" i "slika" i preuzimamo element preko id selektora
    var box = document.getElementById(x);  
    var slika = document.getElementById(y);

    // upravljamo gridom "box" varijable kako bi mogli prosirit dio grida gdje se nalazi slika
    box.style.gridTemplateColumns="1px 1px 1fr "; //  kolone
    box.style.gridTemplateRows=" 1px 1fr 1px 50px 50px 50px "; // redovi

    // sliku prosirujemo i uvecavamo dimenzije kako bi popunila prostor koji smo napravili u prethodnom koraku
    slika.style.width="300px";
    slika.style.height="250px";
}
function smanji(x,y){ // isto kao funkcija povecaj, samo sto sada smanjujemo dimenzije i grid vracamo na prvobitnu poziciju
    var box = document.getElementById(x);
    var slika = document.getElementById(y);
    box.style.gridTemplateColumns="50px 1fr 50px";
    box.style.gridTemplateRows=" 25px 1fr 25px 50px 50px 50px ";
    slika.style.width="200px";
    slika.style.height="200px";
}


//Zadatak 3
function sakrij(a1,a2,a3){ // ova funkcije važi i za button prva godina i za button druga godina tj pozivace se kod tih buttona
    /*Funkcija prima tri parametra, tj tri predmeta iz prve ili druge godine */

    // svaki predmet odnosno parametar nazivamo odredjenom varijablom
    var item1 = document.getElementById(a1);
    var item2 = document.getElementById(a2);
    var item3 = document.getElementById(a3);

    // predmet sakrivamo
    item1.style.display="none";
    item2.style.display="none";
    item3.style.display="none";
}

function prikazi(a1,a2,a3,b1,b2,b3){ /* isto kao i prethodna funkcija*/
    // samo sto sada pozivamo svih 6 predmeta iz stranice predmeti i nazivamo ih odredjenim varijablama
    var item1 = document.getElementById(a1);
    var item2 = document.getElementById(a2);
    var item3 = document.getElementById(a3);
    var item4 = document.getElementById(b1);
    var item5 = document.getElementById(b2);
    var item6 = document.getElementById(b3);
   
    // prikazujemo predmete
    item1.style.display="";
    item2.style.display="";
    item3.style.display="";
    item4.style.display="";
    item5.style.display="";
    item6.style.display="";
}


// Zadatak 2   // -----------NE RADI------------------*/
function sortiraj(a1,a2,a3,b1,b2,b3){
    //var item1 = document.getElementById(a1);
    var item2 = document.getElementById(a2).value;
    var item3 = document.getElementById(a3).value;
    var item4 = document.getElementById(b1).value;
    var item5 = document.getElementById(b2).value;
    var item6 = document.getElementById(b3).value;
    var i=0;
    for(i;i<6;i++){
        if(item2[0]>='A'){
            alert("item[0]");
        }
    }
}